/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springsexperiments.trial.journals.controller;

import com.springexperiments.trial.journals.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 *
 * @author abrahamgimeno
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class JournalControllerTest extends BaseControllerTest{
   

    @Test
    public void renderDocumentSuccessTest() throws Exception{        
        TestingAuthenticationToken testingAuthenticationToken = getUserAuthentication("publisher1");
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/view/{id}", new Long(1)).principal(testingAuthenticationToken))
                .andExpect(status().isOk())
                .andDo(print()) 
                .andReturn();
    }    

    @Test
    public void renderDocumentFailureTest() throws Exception{        
        TestingAuthenticationToken testingAuthenticationToken = getUserAuthentication("publisher2");
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/view/{id}", new Long(1)).principal(testingAuthenticationToken))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();
        
    }
    
}
