/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springsexperiments.trial.journals.controller;

import com.springexperiments.trial.journals.Application;
import com.springexperiments.trial.journals.model.Journal;
import com.springexperiments.trial.journals.repository.JournalRepository;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Abraham Gimeno
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class PublisherControllerTest extends BaseControllerTest {


    @Autowired
    private JournalRepository journalRepository;


    @Before
    @Override
    public void setup() throws Exception {
        super.setup();        
    }

    @Test
    public void handleFileUploadTest() throws IOException, Exception {

        MockMultipartFile file = getMockMultipartFile("classpath:quick-vim.pdf");
        TestingAuthenticationToken testingAuthenticationToken = getUserAuthentication("publisher2");
        Boolean found = false;
        String name = "Test Journal ";

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/publisher/publish")
                .file("file", file.getBytes())
                .principal(testingAuthenticationToken)
                .param("category", "1")
                .param("name", name)
        ).andExpect(redirectedUrl("/publisher/publish")).andReturn().getRequest();

        for (Journal j : journalRepository.findAll()) {
            if (j.getName().equals(name)) {
                found = true;
            }
        }
        assert (found);
    }

    @Test
    public void loadUploadPageTest() throws Exception {
        TestingAuthenticationToken testingAuthenticationToken = getUserAuthentication("publisher2");
        mockMvc.perform(get("/publisher/publish").principal(testingAuthenticationToken)).andExpect(status().isOk());
      
    }

}
