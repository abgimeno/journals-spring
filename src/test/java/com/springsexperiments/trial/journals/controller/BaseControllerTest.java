/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springsexperiments.trial.journals.controller;

import com.springexperiments.trial.journals.config.Configuration;
import com.springexperiments.trial.journals.service.CurrentUser;
import com.springexperiments.trial.journals.service.UserService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author abrahamgimeno
 */
public class BaseControllerTest {
    
    protected MockMvc mockMvc;
        
    @Autowired
    protected UserService userService;
    
    @Autowired
    protected WebApplicationContext webApplicationContext;
    
    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
            
    }    
    
    protected TestingAuthenticationToken getUserAuthentication(String user) {
        CurrentUser activeUser = new CurrentUser(userService.getUserByLoginName(user).get());
        return new TestingAuthenticationToken(activeUser, null);
    }

    protected MockMultipartFile getMockMultipartFile(String resourceName)throws IOException, Exception  {
        File toUpload = webApplicationContext.getResource(resourceName).getFile();
        FileInputStream fi1 = new FileInputStream(toUpload);
        return new MockMultipartFile("file", toUpload.getName(), "multipart/form-data", fi1);
    }    
}
