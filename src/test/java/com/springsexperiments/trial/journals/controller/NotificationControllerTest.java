/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springsexperiments.trial.journals.controller;

import com.springexperiments.trial.journals.controller.NotificationsController;
import com.springexperiments.trial.journals.Application;
import com.springexperiments.trial.journals.exception.NotValidEmailAddressException;
import com.springexperiments.trial.journals.model.Journal;
import com.springexperiments.trial.journals.model.User;
import com.springexperiments.trial.journals.repository.JournalRepository;
import com.springexperiments.trial.journals.repository.UserRepository;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.mail.MessagingException;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author Abraham Gimeno
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class NotificationControllerTest extends BaseControllerTest{

    @Autowired
    private JournalRepository journalRepository;

    @Autowired
    private NotificationsController notificationsController;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void handlePublicationNotifications() {
        try {
            Long categoryId = new Long(1);
            Journal journal = journalRepository.findOne(categoryId);
            notificationsController.handlePublicationNotifications(journal);
        } catch (NotValidEmailAddressException | MessagingException | IOException ex) {
            fail("Notification was not sent ====> "+ex.getMessage());            
        }

    }

    @Test
    public void sendDigestNotification()  {
        try {
            List<Journal> journals = journalRepository.findOnPublishDate(getTodayStartDate(), getTodayEndDate());
            List<User> users = userRepository.findAll();
            notificationsController.sendDigestNotification(users, journals);
        } catch (NotValidEmailAddressException | MessagingException | IOException ex) {
            fail("Notification was not sent");
            System.out.println(ex.getMessage());
        }

    }

    private Date getTodayEndDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        System.out.println(cal.getTime());
        return cal.getTime();
    }

    private Date getTodayStartDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        System.out.println(cal.getTime());
        return cal.getTime();
    }
}
