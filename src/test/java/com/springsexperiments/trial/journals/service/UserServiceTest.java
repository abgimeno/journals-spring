/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springsexperiments.trial.journals.service;

import com.springexperiments.trial.journals.service.UserService;
import com.springexperiments.trial.journals.Application;
import com.springexperiments.trial.journals.model.User;
import com.springexperiments.trial.journals.model.Category;
import com.springexperiments.trial.journals.model.Subscription;
import com.springexperiments.trial.journals.repository.CategoryRepository;
import com.springexperiments.trial.journals.repository.SubscriptionRepository;
import com.springexperiments.trial.journals.repository.UserRepository;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author Abraham Gimeno
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private UserService userService;

    @Before
    public void setup() throws Exception {

    }

    @Test
    public void getUserByLoginNameTest() {
        User user = userRepository.findByLoginName("publisher1");
        assertEquals(user.getLoginName(),"publisher1");
        user = userRepository.findByLoginName("publisher2");
        assertEquals(user.getLoginName(),"publisher2");
        user = userRepository.findByLoginName("user1");
        assertEquals(user.getLoginName(),"user1");
        user = userRepository.findByLoginName("user2");
        assertEquals(user.getLoginName(),"user2");
    }

    @Test
    public void subscribe() {
        Boolean userfound =false;
        User user = userRepository.findByLoginName("publisher1");
        Long id = new Long(1);        
        Category cat = categoryRepository.findOne(id);
        userService.subscribe(user, id);
        List<Subscription> subscriptions = subscriptionRepository.findByCategory(cat);
        for(Subscription s: subscriptions){
            if (s.getUser().getId().equals(user.getId())){
                userfound= true;
            }
        }
        if(!userfound){
            fail("Subscribed user was not found");
        }
    }

    @Test
    public void findById() {
        Long id = new Long(1);  
        User user = userRepository.findOne(id);
        assertEquals(user.getLoginName(),"publisher1");
        id = new Long(2);
        user = userRepository.findOne(id);
        assertEquals(user.getLoginName(),"publisher2");
        id = new Long(3);
        user = userRepository.findOne(id);
        assertEquals(user.getLoginName(),"user1");
        id = new Long(4);
        user = userRepository.findOne(id);
        assertEquals(user.getLoginName(),"user2");
    }

}
