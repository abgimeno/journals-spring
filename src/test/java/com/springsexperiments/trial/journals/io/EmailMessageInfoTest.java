/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springsexperiments.trial.journals.io;

import com.springexperiments.trial.journals.io.EmailMessageInfo;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import com.springexperiments.trial.journals.exception.NotValidEmailAddressException;

/**
 *
 * @author abrahamgimeno
 */
public class EmailMessageInfoTest {

    private static final String INVALID_ADDRESS1 = "sdkfjh3";
    private static final String INVALID_ADDRESS2 = "sdkfjh3@";
    private static final String INVALID_ADDRESS3 = "abraham@gimeno@tonm";
    private static final String INVALID_ADDRESS4 = "=sas2.co@com.cs";
    private static final String VALID_ADDRESS1 = "abraham@euler.com";
    private static final String VALID_ADDRESS2 = "tfl@tfl.gov.uk";
    private static final String VALID_ADDRESS3 = "tfl.helpdesk@tfl.gov.uk";
    private static final String VALID_ADDRESS4 = "jose@gob.es";

    private List<String> invalid;
    private List<String> valid;

    public EmailMessageInfoTest() {
        
    }
    
    @Before
    public void setup() throws Exception {
        initInvalidList();
        initValidList();
    }

    /**
     * Test of setFrom method, of class EmailMessageInfo.
     */
    @Test
    public void testSetInvalidFrom() {

        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setFrom(INVALID_ADDRESS1);
            message.setFrom(INVALID_ADDRESS2);
            message.setFrom(INVALID_ADDRESS3);
            message.setFrom(INVALID_ADDRESS4);
            fail("Invalid email address accepted");
        } catch (NotValidEmailAddressException ex) {

        }

    }
    public void testSetValidFrom() {

        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setFrom(VALID_ADDRESS1);
            message.setFrom(VALID_ADDRESS2);
            message.setFrom(VALID_ADDRESS3);
            message.setFrom(VALID_ADDRESS4);
            
        } catch (NotValidEmailAddressException ex) {
            fail("Valid email address rejected");
        }

    }
    /**
     * Test of setTo method, of class EmailMessageInfo.
     */
    @Test
    public void testSetInvalidTo() {
        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setTo(INVALID_ADDRESS1);
            message.setTo(INVALID_ADDRESS2);
            message.setTo(INVALID_ADDRESS3);
            message.setTo(INVALID_ADDRESS4);
            fail("Invalid email address accepted");
        } catch (NotValidEmailAddressException ex) {

        }
    }
    /**
     * Test of setTo method, of class EmailMessageInfo.
     */
    @Test
    public void testValidSetTo() {
        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setTo(VALID_ADDRESS1);
            message.setTo(VALID_ADDRESS2);
            message.setTo(VALID_ADDRESS3);
            message.setTo(VALID_ADDRESS4);
            
        } catch (NotValidEmailAddressException ex) {
            fail("Valid email address rejected");
        }
    }
    /**
     * Test of setBcc method, of class EmailMessageInfo.
     */
    @Test
    public void testInvalidSetBcc() {
        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setBcc(invalid);
            fail("Invalid email address accepted");
        } catch (NotValidEmailAddressException ex) {

        }

    }
    /**
     * Test of setBcc method, of class EmailMessageInfo.
     */
    @Test
    public void testValidSetBcc() {
        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setBcc(valid);            
        } catch (NotValidEmailAddressException ex) {
            fail("Valid email address rejected");
        }

    }
    /**
     * Test of setCc method, of class EmailMessageInfo.
     */
    @Test
    public void testInvalidSetCc() {
        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setCc(invalid);
            fail("Invalid email address accepted");
        } catch (NotValidEmailAddressException ex) {

        }
    }
    /**
     * Test of setCc method, of class EmailMessageInfo.
     */
    @Test
    public void testValidSetCc() {
        EmailMessageInfo message = new EmailMessageInfo();
        try {
            message.setCc(valid);            
        } catch (NotValidEmailAddressException ex) {
            fail("Valid email address rejected");
        }
    }

    private void initInvalidList() {
        invalid = new ArrayList<>();
        invalid.add(INVALID_ADDRESS1);
        invalid.add(INVALID_ADDRESS2);
        invalid.add(INVALID_ADDRESS3);
        invalid.add(INVALID_ADDRESS4);
        
    }

    private void initValidList() {
        valid = new ArrayList<>();
        valid.add(VALID_ADDRESS1);
        valid.add(VALID_ADDRESS2);
        valid.add(VALID_ADDRESS3);
        valid.add(VALID_ADDRESS4);
    }

}
