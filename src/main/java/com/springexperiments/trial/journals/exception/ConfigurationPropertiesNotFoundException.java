/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.exception;

/**
 *
 * @author abrahamgimeno
 */
public class ConfigurationPropertiesNotFoundException extends Exception {

    public ConfigurationPropertiesNotFoundException(String message) {
        super(message);
    }

    public ConfigurationPropertiesNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
