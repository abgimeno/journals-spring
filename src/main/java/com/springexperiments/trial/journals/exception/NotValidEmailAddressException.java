/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.exception;

import java.util.List;

/**
 *
 * @author Abraham Gimeno
 */
public class NotValidEmailAddressException extends Exception {

    public NotValidEmailAddressException(String message) {
        super(message);
    }

    public  NotValidEmailAddressException(String message, List<String> rejected) {
             
    }
    
}
