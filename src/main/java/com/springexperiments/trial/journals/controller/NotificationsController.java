/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.controller;

import com.springexperiments.trial.journals.Application;
import com.springexperiments.trial.journals.config.Configuration;
import com.springexperiments.trial.journals.exception.ConfigurationPropertiesNotFoundException;
import com.springexperiments.trial.journals.io.EmailMessageInfo;
import com.springexperiments.trial.journals.exception.NotValidEmailAddressException;
import com.springexperiments.trial.journals.io.SendMail;
import com.springexperiments.trial.journals.model.Journal;
import com.springexperiments.trial.journals.model.User;
import com.springexperiments.trial.journals.service.UserService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 *
 * @author abrahamgimeno
 */
@Component
public class NotificationsController {

    @Autowired
    public UserService userService;

    @Autowired
    private ApplicationContext ctx;
    
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private final Logger log = org.apache.log4j.Logger.getLogger(NotificationsController.class);
    
    /*
    handles notificaions after publication
    scheduling of daily notifications and sending immediately notification for subscribers
     */
    public void handlePublicationNotifications(Journal journal) throws MessagingException, AddressException, UnsupportedEncodingException, NotValidEmailAddressException, IOException {
        
        List<User> users = userService.getUsersBySubscription(journal.getCategory().getId());
        if (!users.isEmpty()) {
            sendNotificationToSubscribers(journal, users);
        }
    }

    /*
    notification to be sent upon publication
     */
    private void sendNotificationToSubscribers(Journal journal, List<User> users) throws MessagingException, AddressException, UnsupportedEncodingException, NotValidEmailAddressException, IOException {

        EmailMessageInfo emailMessage = new EmailMessageInfo();
        emailMessage.setFrom("crossover@eulercomputing.com");
        emailMessage.setTo("crossover@eulercomputing.com");
        emailMessage.setBody("The following file has been published. Please log in to view it.");
        emailMessage.setSubject("New publication " + journal.getName());
        for (User u : users) {
            emailMessage.getBcc().add(u.getEmail());
        }
        SendMail.sendMail(emailMessage, getEmailProperties());
        log.info("[DEBUG]-- handlePublicationNotifications sendEmail called");

    }

    /*
    method for sending daily digest
    needs list of users and list of journals
     */
    public void sendDigestNotification(List<User> users, List<Journal> journals) throws MessagingException, AddressException, UnsupportedEncodingException, NotValidEmailAddressException, IOException {
        if (!journals.isEmpty()) {

            String body = createDailyNoficationMessageBody(journals);
            String subject = "Daily journal publication digest " + dateFormat.format(new Date());
            EmailMessageInfo emailMessage = new EmailMessageInfo();
            emailMessage.setFrom("crossover@eulercomputing.com");
            emailMessage.setTo("crossover@eulercomputing.com");
            emailMessage.setBody(body);
            emailMessage.setSubject(subject);
            for (User u : users) {
                emailMessage.getBcc().add(u.getEmail());
            }
            SendMail.sendMail(emailMessage, getEmailProperties());
            log.info("[DEBUG]-- sendDailyNotification sendEmail called");

        }
    }

    private String createDailyNoficationMessageBody(Iterable<Journal> journals) {
        StringBuilder body = new StringBuilder("The following journals have been published today. Please log in to view it.<br>");
        for (Journal j : journals) {
            body.append("<br>---------------------------<br>");
            body.append("Journal name: ");
            body.append(j.getName());
            body.append("<br>Journal publisher: ");
            body.append(j.getPublisher().getName());
            body.append("<br>---------------------------<br>");
        }
        log.info("Message Body:");
        log.info(body.toString());
        return body.toString();
    }

    private Properties getEmailProperties() throws IOException {
        
        try {
             return Configuration.getInstance().getProperties(Application.MAIL_PROPERTIES);
        } catch (ConfigurationPropertiesNotFoundException ex) {              
            Resource template = ctx.getResource(Application.MAIL_PROPERTIES);
            File mail = template.getFile();
            Properties props = new Properties();
            props.load(new FileInputStream(mail));
            Configuration.getInstance().addProperties(Application.MAIL_PROPERTIES, props);
            return props;
        }
    }
        
}


