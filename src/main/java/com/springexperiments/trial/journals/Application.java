package com.springexperiments.trial.journals;

import com.springexperiments.trial.journals.config.Configuration;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {

    public static final String ROOT;
    public static final String MAIL_PROPERTIES = "classpath:mail.properties";
    /**
     * email properties
     */

    public static Properties mailprops; 
    
        
    static {
        ROOT = System.getProperty("upload-dir", System.getProperty("user.home") + "/upload");
    }

    public static void main(String[] args) throws Exception {
        
        try {
            ApplicationContext ctx = SpringApplication.run(Application.class, args);
            Resource template = ctx.getResource(MAIL_PROPERTIES);
            File mail = template.getFile();
            mailprops = new Properties();
            mailprops.load(new FileInputStream(mail));
            Configuration.getInstance().addProperties(MAIL_PROPERTIES, mailprops);
        } catch (IOException ex) {
            org.apache.log4j.Logger.getLogger(Application.class).error("Failed to load email configuration file, aborting", ex);
        }
    }

}
