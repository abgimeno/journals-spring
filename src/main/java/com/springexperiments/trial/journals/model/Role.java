package com.springexperiments.trial.journals.model;

public enum Role {
	USER, PUBLISHER
}