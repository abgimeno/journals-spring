package com.springexperiments.trial.journals.service;

import com.springexperiments.trial.journals.exception.ServiceException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.springexperiments.trial.journals.model.Category;
import com.springexperiments.trial.journals.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springexperiments.trial.journals.model.Subscription;
import com.springexperiments.trial.journals.model.User;
import com.springexperiments.trial.journals.repository.SubscriptionRepository;
import com.springexperiments.trial.journals.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
        
        @Autowired
	private CategoryRepository categoryRepository;
        
        @Autowired
	private SubscriptionRepository subscriptionRepository;        

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
 
	@Override
	public Optional<User> getUserByLoginName(String loginName) {
		return Optional.ofNullable(userRepository.findByLoginName(loginName));
	}

	@Override
	public void subscribe(User user, Long categoryId) {
		List<Subscription> subscriptions;
		subscriptions = user.getSubscriptions();
		if (subscriptions == null) {
			subscriptions = new ArrayList<>();
		}
		Optional<Subscription> subscr = subscriptions.stream()
				.filter(s -> s.getCategory().getId().equals(categoryId)).findFirst();
		if (!subscr.isPresent()) {
			Subscription s = new Subscription();
			s.setUser(user);
			Category category = categoryRepository.findOne(categoryId);
			if(category == null) {
				throw new ServiceException("Category not found");
			}
			s.setCategory(category);
			subscriptions.add(s);
			userRepository.save(user);
		}
	}

	@Override
	public User findById(Long id) {
		return userRepository.findOne(id);
	}
        
        @Override
        public List<User> getUsersBySubscription (Long categoryId){
            Category c = categoryRepository.getOne(categoryId);
            List<User> users = new ArrayList<>();
            List<Subscription> subscriptions = subscriptionRepository.findByCategory(c);            
            for (Subscription s:subscriptions){
                if(!users.contains(s.getUser())){
                    users.add(s.getUser());
                }
            }
            return users;
        }

}
