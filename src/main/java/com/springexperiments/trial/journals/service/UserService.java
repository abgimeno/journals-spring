package com.springexperiments.trial.journals.service;

import java.util.Optional;

import com.springexperiments.trial.journals.model.User;
import java.util.List;

public interface UserService {

    Optional<User> getUserByLoginName(String loginName);

    void subscribe(User user, Long categoryId);

    User findById(Long id);
    
    List<User> getUsersBySubscription (Long category);
    
}