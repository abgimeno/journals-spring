/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.config;

import com.springexperiments.trial.journals.exception.ConfigurationPropertiesNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.springframework.stereotype.Component;

/**
 *
 * Singleton to store configurationdata
 *
 *
 * @author abraham
 */
@Component
public final class Configuration {


    
    private static final Map<String, Properties> properties = new HashMap<>();  
 
    /**
     * Singleton instance
     */
    private static Configuration singleton = null;

    protected Configuration() throws IOException {
        
    }

    public static Configuration initInstance() throws IOException {
        if (singleton == null) {
            singleton = new Configuration();
        }
        return singleton;
    }

    public static Configuration getInstance() throws IOException {
        if (singleton == null) {
            initInstance();
        }
        return singleton;    }

    public static String getProperty(String propertiesName, String propertyName) {
        return properties.get(propertyName).getProperty(propertyName);
    }

    public Properties getProperties(String propertiesName) throws ConfigurationPropertiesNotFoundException {
        if(!properties.containsKey(propertiesName)){
            throw new ConfigurationPropertiesNotFoundException(propertiesName);
        }
        return properties.get(propertiesName);
    }
    public void addProperties (String resourceName, Properties props){
        properties.put(resourceName,props);
    }

}
