/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.io;

import com.springexperiments.trial.journals.exception.NotValidEmailAddressException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 *
 * @author abraham
 *
 */
@Embeddable
public class EmailMessageInfo implements Serializable {

    @Column(name = "BODY")
    private String body;

    @Column(name = "FROM_ADDRESS")
    private String fromAddress;

    @Column(name = "TO_ADDRESS")
    private String toAddress;

    @Column(name = "SUBJECT")
    private String subject;

    private List<String> cc = new ArrayList<>();

    private List<String> bcc = new ArrayList<>();

    @Transient
    private static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@(.+)$";

    @Transient
    private Pattern p;

    @Transient
    private Matcher m;

    public EmailMessageInfo() {
        p = Pattern.compile(EMAIL_REGEX);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFrom() {
        return fromAddress;
    }

    public void setFrom(String from) throws NotValidEmailAddressException {
        m = p.matcher(from);
        if (m.matches()) {
            this.fromAddress = from;
        } else {
            throw new NotValidEmailAddressException("The address " + from + " is not a valid email address");
        }
    }

    public String getTo() {
        return toAddress;
    }

    public void setTo(String to) throws NotValidEmailAddressException {
        m = p.matcher(to);
        if (m.matches()) {
            this.toAddress = to;
        } else {
            throw new NotValidEmailAddressException("The address " + to + " is not a valid email address");
        }
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) throws NotValidEmailAddressException {
        List<String> rejected = new ArrayList<>();
        for (String s : bcc) {
            m = p.matcher(s);
            if (m.matches()) {
                this.bcc.add(s);
            }else{
                rejected.add(s);
            }
        }       
        
        if (!rejected.isEmpty()){
           throw new NotValidEmailAddressException(createAddressesRejectedMessage("Email addresses were rejected ", rejected));

        }
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) throws NotValidEmailAddressException {
        List<String> rejected = new ArrayList<>();
        for (String s : cc) {
            m = p.matcher(s);
            if (m.matches()) {
                this.cc.add(s);
            }else{
                rejected.add(s);                
            }               
        if (!rejected.isEmpty()){
           throw new NotValidEmailAddressException("Email addresses were rejected ", rejected);
            }
        }
    }
    private String createAddressesRejectedMessage(String message, List<String> rejected) {
        StringBuilder composedMessage = new StringBuilder(message);
        composedMessage.append("\n");
        for (String s : rejected) {
            composedMessage.append(s);
            composedMessage.append("; ");
        }
        composedMessage.append("\n");
        return composedMessage.toString();
    }

}
