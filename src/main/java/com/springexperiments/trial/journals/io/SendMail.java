/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.io;

import java.io.UnsupportedEncodingException;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/**
 * Send a mail using java mail api
 *
 * @author abraham
 */
public class SendMail {

    public static String MAILHOST = "mail.smtps.host";
    public static String MAILFROM = "mail.from";
    public static String MAILPASS = "mail.password";

    public static void sendMail(EmailMessageInfo emailInfo, Properties props) throws AddressException, MessagingException, UnsupportedEncodingException {

        Authenticator auth = new SimpleAuthenticator(props.getProperty(MAILFROM), props.getProperty(MAILPASS));
        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);

        InternetAddress afrom = new InternetAddress(emailInfo.getFrom());
        InternetAddress ato = new InternetAddress(emailInfo.getTo());

        msg.setFrom(afrom);
        msg.addRecipient(Message.RecipientType.TO, ato);
        populateCc(emailInfo, msg);
        populateBcc(emailInfo, msg);

        msg.setSubject(MimeUtility.encodeText(emailInfo.getSubject(), "UTF-8", null));

        Multipart mp = new MimeMultipart();

        // TEXT PART
        BodyPart textPart = new MimeBodyPart();
        textPart.setText(emailInfo.getBody()); // sets type to "text/plain"

        //HTML PART
        BodyPart pixPart = new MimeBodyPart();

        //pixPart.setContent(MimeUtility.encodeText(emailInfo.getBody(), "UTF-8", null), "text/html; charset=utf-8");
        pixPart.setContent(emailInfo.getBody(), "text/html; charset=utf-8");

        // Collect the Parts into the MultiPart
        //mp.addBodyPart(textPart);
        mp.addBodyPart(pixPart);
        msg.setContent(mp);
        sendEmail(msg, session, props);

    }

    private static void populateCc(EmailMessageInfo emailInfo, MimeMessage msg) throws MessagingException {
        if (!emailInfo.getCc().isEmpty()) {
            InternetAddress[] adresses = new InternetAddress[emailInfo.getCc().size()];
            for (int i = 0; i < emailInfo.getCc().size(); i++) {
                InternetAddress acc = new InternetAddress(emailInfo.getCc().get(i));
                adresses[i] = acc;
            }
            msg.setRecipients(Message.RecipientType.CC, adresses);
        }
    }

    private static void populateBcc(EmailMessageInfo emailInfo, MimeMessage msg) throws AddressException, MessagingException {
        if (!emailInfo.getBcc().isEmpty()) {            
            InternetAddress[] adresses = new InternetAddress[emailInfo.getBcc().size()];
            for (int i = 0; i < emailInfo.getBcc().size(); i++) {
                InternetAddress acc = new InternetAddress(emailInfo.getBcc().get(i));
                adresses[i] = acc;
            }
            msg.setRecipients(Message.RecipientType.BCC, adresses);

        }
    }

    private static void sendEmail(Message msg, Session session, Properties props) throws MessagingException {
        Transport transport = session.getTransport();
        transport.connect(props.getProperty(MAILHOST), props.getProperty(MAILFROM), props.getProperty(MAILPASS));
        transport.sendMessage(msg, msg.getAllRecipients());
        Logger.getLogger(SendMail.class.getName()).log(Level.INFO, "SendMail::SendMail() mail sent");
    }

}
