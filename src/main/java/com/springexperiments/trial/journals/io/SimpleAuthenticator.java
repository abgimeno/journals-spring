/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.springexperiments.trial.journals.io;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 *
 * @author abraham
 */
public class SimpleAuthenticator extends Authenticator{

    private final String username;
    private final String password;

    public SimpleAuthenticator(String username, String password) {
        this.username=username;
        this.password=password;
    }



  @Override
  public PasswordAuthentication getPasswordAuthentication() {
    return new PasswordAuthentication(username, password);
  }



    
}
