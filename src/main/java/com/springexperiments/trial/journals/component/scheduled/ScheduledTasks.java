package com.springexperiments.trial.journals.component.scheduled;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Abraham Gimeno
 */

import com.springexperiments.trial.journals.controller.NotificationsController;
import com.springexperiments.trial.journals.exception.NotValidEmailAddressException;
import com.springexperiments.trial.journals.model.User;
import com.springexperiments.trial.journals.model.Journal;
import com.springexperiments.trial.journals.repository.JournalRepository;
import com.springexperiments.trial.journals.repository.UserRepository;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
  
    @Autowired
    public UserRepository userRepository;
    
    @Autowired
    public JournalRepository journalRepository;
    
    @Autowired
    public NotificationsController notificationController;
    
    private final static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ScheduledTasks.class);
        
    @Scheduled(fixedRate = 2400000/*cron = "0 0 6 * * *"*/)
    public void reportDailyJournalPublications() {
        try {            
            log.info( "Scheduled task execution start");
            List<User> users = userRepository.findAll();
            Date startDate = getStartDate();
            Date endDate = getEndDate();
            List<Journal> journals = journalRepository.findOnPublishDate(startDate, endDate);
            notificationController.sendDigestNotification(users, journals);
            log.info("Scheduled task execution end");
        } catch (IOException | MessagingException | NotValidEmailAddressException ex) {
            log.error(ex);
        }
    }
    private Date getEndDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);        
        System.out.println(cal.getTime());
        return cal.getTime();
    }
    private Date getStartDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);        
        System.out.println(cal.getTime());
        return cal.getTime();
    }    
}
