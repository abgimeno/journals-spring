package com.springexperiments.trial.journals.repository;

import com.springexperiments.trial.journals.model.Journal;
import com.springexperiments.trial.journals.model.Publisher;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalRepository extends JpaRepository<Journal, Long> {

    Collection<Journal> findByPublisher(Publisher publisher);

    List<Journal> findByCategoryIdIn(List<Long> ids);
    
    List<Journal> findOnPublishDate(Date startDate, Date endDate);

}
