package com.springexperiments.trial.journals.repository;

import com.springexperiments.trial.journals.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {    

}
