/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springexperiments.trial.journals.repository;

import com.springexperiments.trial.journals.model.Category;
import com.springexperiments.trial.journals.model.Subscription;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Abraham Gimeno
 */
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    List<Subscription> findByCategory(Category category);
}
